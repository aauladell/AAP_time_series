# Long-term seasonality of marine photoheterotrophic bacteria reveals low cohesiveness within the different phylogroups 

The scripts for inicial pre-processing of the data and statistical analysis can be found in the `scripts` folder.

The versions for each program are the following:

- `cutadapt` version 1.14. 
- `fastLSA` version 1.0
- `USEARCH` version 8.1.1756 (64 bits)
- `BLAST` version 2.6.0

The versions of R and the used packages can be found here:


```
R version 3.4.3 (2017-11-30)
Platform: x86_64-apple-darwin15.6.0 (64-bit)
Running under: macOS High Sierra 10.13.3

Matrix products: default
BLAS: /Library/Frameworks/R.framework/Versions/3.4/Resources/lib/libRblas.0.dylib
LAPACK: /Library/Frameworks/R.framework/Versions/3.4/Resources/lib/libRlapack.dylib

locale:
[1] ca_ES.UTF-8/ca_ES.UTF-8/ca_ES.UTF-8/C/ca_ES.UTF-8/ca_ES.UTF-8

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base

other attached packages:
 [1] mice_2.46.0         phyloseq_1.22.3     vegan_2.4-4
 [4] lattice_0.20-35     permute_0.9-4       tidygraph_1.0.0
 [7] ggraph_1.0.0        GeneCycle_1.1.2     fdrtool_1.2.15
[10] longitudinal_1.1.12 corpcor_1.6.9       MASS_7.3-47
[13] dada2_1.4.0         Rcpp_0.12.16        forcats_0.2.0
[16] stringr_1.3.0       dplyr_0.7.4         purrr_0.2.4
[19] readr_1.1.1         tidyr_0.7.2         tibble_1.4.2
[22] ggplot2_2.2.1.9000  tidyverse_1.2.1
```
