#!/bin/sh

for i in *R1_001.fastq.gz; 
do
SAMPLE=$(echo ${i} | sed "s/R1_001\.fastq\.gz//");
cutadapt -a TACGGSAACCTGTWCTAC...TGGGCNTGGTGGTTYGCNRA \
         -A AYNGCRAACCACCANGCCCA...GTAGWACAGGTTSCCGTA \
          ${SAMPLE}R1_001.fastq.gz ${SAMPLE}R2_001.fastq.gz \
         -o trimmed_reads/${SAMPLE}R1_001.fastq.gz -p \
        trimmed_reads/${SAMPLE}R2_001.fastq.gz \
        --minimum-length 80;
done
